import React, { Component } from 'react';
import Navigation from './components/Footer/Navigation';

class Footer extends Component {
    state = {  }
    render() { 
        const { mobile, pages, onClick, navClick } = this.props;
        return ( 
            <footer>
                <Navigation 
                    mobile={mobile}
                    pages={pages}
                    onClick={onClick}
                />
            </footer>
         );
    }
}
 
export default Footer;
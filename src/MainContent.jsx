import React, { Component } from 'react';
import About from './components/Main/About/About'
import Projects from './components/Main/Projects/Projects'

class MainContent extends Component {
    state = {  }

    showPage(){
        const { pages, winStat, panelStat, onClose } = this.props;
        if( pages[0].det || pages[0].desc ){
            const {id, page, det, desc} = pages[0];
            return(
                this.showAbout()
            )
        }
        else if( pages[1].det || pages[1].desc ){
                return(
                    this.showProjects()
                )
        }
    }

    showAbout(){
        const { pages, winStat, panelStat, viewStack, onClose, onIconClick, setToTop } = this.props;
        const {id, page, det, desc} = pages[0];
        const top = viewStack[id];
        return(
            <About det={det} desc={desc} id={id} page={page} top={top} onClose={onClose} onIconClick={onIconClick} setToTop={setToTop}/>
        );
    }

    showProjects(){
        const { pages, winStat, viewStack, panelStat, onClose, onIconClick, setToTop } = this.props;
        const {id, page, det, desc} = pages[1];
        const top = viewStack[id];
        return(
            <Projects det={det} desc={desc} id={id} page={page} top={top} onClose={onClose} onIconClick={onIconClick} setToTop={setToTop}/>
        );
    }

    render() { 
        return ( 
            <main>
                {/* {this.showPage()} */}
                {this.showAbout()}
                {this.showProjects()}
            </main>
         );
    }
}
 
export default MainContent;
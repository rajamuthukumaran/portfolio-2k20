import React from 'react';
import Status from './components/Header/Status'
import News from './components/Header/News'

const Header = () => {
    return ( 
        <>
            <Status/>
            <News/>
        </>
     );
}
 
export default Header;
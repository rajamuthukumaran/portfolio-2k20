import React, { Component } from "react";
import Header from "./Header";
import MainContent from "./MainContent";
import Overlay from "./components/Overlay";
import "./scss/App.scss";
import Footer from "./Footer";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pages: [
        { id: 0, page: "about", det: true, desc: true},
        { id: 1, page: "projects", det: false, desc: false },
        { id: 2, page: "git", det: false, desc: false },
        { id: 3, page: "games", det: false, desc: false }
      ],
      window: true,
      active_win:[1,2,3,4],
      panel: true,
      active_pnl: 0,
      mobile: false
    };

    this.handleClose = this.handleClose.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.manualUpdate = this.manualUpdate.bind(this);
    this.getInFront = this.getInFront.bind(this);
    this.manualGetInFront = this.manualGetInFront.bind(this);
  }

  handleClose(event) {
    const ref = event.target.className;
    const obj = ref.split(" ");
    const id = obj[0];
    const ele = obj[1];
    console.log(ele);
    this.setState(prevState => {
      const new_page = prevState.pages.map(i => {
        if (id == i.id) {
          if (ele == "det") {
            i.det = false;
          } else {
            i.desc = false;
          }
        }
        return i;
      });
      let win, pnl;
      if( ele == "det" ){
        win = false;
        pnl = prevState.panel;
      }else{
        win = prevState.win;
        pnl = false;
      }
      return {
        pages: new_page,
        window: win,
        panel: pnl
      };
    });
    console.log(this.state.pages);
  }

  handleClick(event) {
    const ref = event.target.className;
    const obj = ref.split(" ");
    const id = obj[0];
    console.log(id);
    this.setState(prevState => {
      const new_page = prevState.pages.map(i => {
        if (id == i.id) {
          i.det = !i.det;
          if (id == 0 && i.det) {
            this.manualUpdate(0,1,true);
          }
        }
        return i;
      });
      return {
        pages: new_page
      };
    });
    this.manualGetInFront(id);
  }

  manualGetInFront(pos){
    const val = this.state.active_win[pos];
    if( val != 1 ){
      this.setState(prevState => {
        const update = prevState.active_win.map( i => {
          if ( i == val ){
            i = 1;
          }else if ( i < val ){
            i++;
          }
          return i;
        })
        return { 
          active_win: update
        };
      });
    }
  }

  getInFront(event){
    const ref = event.target.className;
    const obj = ref.split(" ");
    const pos = obj[0];
    console.log(pos)
    const val = this.state.active_win[pos];
    if( val != 1 ){
      this.setState(prevState => {
        const update = prevState.active_win.map( i => {
          if ( i == val ){
            i = 1;
          }else if ( i < val ){
            i++;
          }
          return i;
        })
        return { 
          active_win: update
        };
      });
    }
  }

  manualUpdate(id,type,flag){
    if(type == 0){
      this.setState(prevState => {
        const update = prevState.pages.map(i => {
          if ( id == i.id ){
            i.det = flag;
          }
          return i;
        });
        return{
          pages: update
        }
      });
    }
    else if( type == 1 ){
      if(this.state.active_pnl != id){
        this.setState(prevState => {
          const update = prevState.pages.map(i => {
            if ( id == i.id ){
              i.desc = flag;
            }
            
            if( this.state.active_pnl == i.id ){
              i.desc = false;
            }
            return i;
          });
          return{
            pages: update,
            active_pnl: id
          }
        });
      }
    }
  }

  render() {
    const { pages, window, active_win, panel, mobile } = this.state;

    return (
      <>
        <Overlay />
        <Header />
        <MainContent
          onClose={this.handleClose}
          pages={pages}
          winStat={window}
          viewStack={active_win}
          panelStat={panel}
          mobile={mobile}
          onIconClick={this.manualUpdate}
          getToTop={this.getInFront}
          setToTop={this.manualGetInFront}
        />
        <Footer pages={pages} mobile={mobile} onClick={this.handleClick}/>
      </>
    );
  }
}

export default App;

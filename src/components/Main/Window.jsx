import React from "react";
import Button from "../Button";
import "../../scss/Window.scss";
import Draggable from 'react-draggable';

const Window = props => {
  const { name, children, onClose, enable, top, getToTop, size, id } = props;
  const temp = id+" win win_" + name + enable ? "" : " hide";
  let hide = '';
  if(!enable){
      hide = ' hide';
  }

  const winpos = {
    zIndex: 1000-top
  }
  return (
    <Draggable>
      <div style={winpos} className={id+" win win_"+name+hide} onDrag={getToTop} onMouseDown={getToTop} onClick={getToTop}>
        <Button
          name="win_clse"
          onBtnClick={onClose}
          id={id}
          size={15}
          shape={'round'}
          type='det'
          btn_img={require("../../assets/img/close.svg")}
        />
        {children}
      </div>
    </Draggable>
  );
};

export default Window;

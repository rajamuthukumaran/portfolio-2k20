import React, { Component } from 'react';
import Panel from './../Panel';
import Window from '../Window'
import RMK from "../../../assets/img/rmk.jpg";
import '../../../scss/About.scss'

class About extends Component {
    constructor(props){
        super(props);
        this.state = { 
            name: 'Rajamuthukumaran D',
            dob: new Date(1996,11,9,9,30),
            age: 22,
            location: 'Chennai',
            email: 'rajamuthukumaran@protonmail.com'
        }
        this.getToTop = this.getToTop.bind(this);
        this.showDesc = this.showDesc.bind(this);
    }

     componentDidMount(){
        const dob = this.state.dob;
        const tdy = Date.now();
        const age = Math.floor((tdy - dob)/31556952000);
        this.setState({
          age: age
        })
     }

     getToTop(){
         const { id, setToTop } = this.props;
         setToTop(id);
     }

     showDetail(){
         const { name, age, location } = this.state;
         const { id, page, onClose, det, top } = this.props;
         return(
             <Window name={page} id={id} enable={det} onClose={onClose} top={top} getToTop={this.getToTop}>
                 <div className="details">
                    <img src={RMK} alt={name} onClick={this.showDesc}/>
                    <div className="info">
                        <p><span>Name : </span>{name}</p>
                        <p><span>Age : </span>{age}</p>
                        <p><span>Gender : </span>Male</p>
                        <p><span>Location : </span>{location}</p>
                        {/* <p><span>Email : </span><a href="">click here</a></p> */}
                    </div>
                </div>
             </Window>
         )
     }

     showDesc(){
        this.props.onIconClick(this.props.id,1,true)
     }

     showDescription(){
        const { name } = this.state;
        const { id, page, onClose, desc } = this.props;
        return(
            <Panel
                name={page}
                id={id}
                onClose={onClose}
                enable={desc}
            >
                <h1 className="name">{name}</h1>
                Hi this is {name},
                <p>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Quibusdam vero necessitatibus natus reiciendis distinctio quas voluptates perspiciatis mollitia deserunt nemo, quidem accusamus minima iusto veniam consequatur eveniet? Accusamus, corrupti enim.
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid accusantium beatae quia eos recusandae nihil fugiat earum quas quod dolore eaque fuga amet velit minus quaerat, eius, autem molestiae quam.
                    Temporibus sit sed laborum? Et, laudantium magni? Et iste possimus modi beatae repellat, dolorum sapiente molestiae culpa in quos, corrupti voluptates consectetur eveniet omnis doloremque vitae qui enim magnam explicabo.
                    Fugit facilis debitis dolor, hic quaerat tenetur excepturi at expedita aspernatur eligendi modi, laboriosam odio ad ipsum nemo culpa quasi autem aliquam ab, sint delectus accusamus? Itaque libero tempora fugiat?
                    Officiis repellat ullam accusantium consequatur ab beatae minima iusto culpa similique totam! Saepe vitae ipsum quibusdam placeat est quisquam, enim hic, voluptatibus voluptas sed nesciunt atque, mollitia assumenda cum minima!
                    Commodi iusto cumque quo totam possimus aperiam enim optio rerum corrupti dolorem dolorum vero minima quam, consequatur explicabo ut et quae quidem ipsum repellendus! Aliquam quasi cumque voluptatum facere in!
                    Sit ipsa velit exercitationem vitae doloremque obcaecati quis est, ex quod nam aliquid similique, nobis aperiam repellat minus modi saepe eligendi aspernatur distinctio reiciendis amet impedit nisi voluptatibus! Numquam, sequi?
                    Totam, deserunt iusto vel esse modi exercitationem itaque officia porro earum magnam reiciendis quod. Esse labore nesciunt totam dolores repudiandae natus exercitationem! Eius distinctio aperiam omnis officiis, ratione fugiat incidunt.
                    Atque adipisci beatae architecto maxime dolorem repudiandae, velit nam aperiam quam unde veritatis accusantium molestiae vitae consectetur magni neque. Consectetur veritatis odit perferendis qui non aliquid possimus eaque eligendi tempore.
                    Vel eius in necessitatibus modi, laboriosam deserunt! Ad dolorem, dignissimos et odio aspernatur magni molestias qui facilis, omnis, asperiores quisquam minus repellat recusandae cumque consectetur doloribus nisi sapiente quae aliquid.
                    Similique non architecto voluptatem quasi quos nulla necessitatibus asperiores doloremque iste aliquam in nam quae animi voluptas, quidem accusamus cupiditate molestias eveniet laborum dolore. Ipsam deserunt eos atque perferendis animi.
                    Necessitatibus unde molestiae molestias vero officia earum quisquam inventore quos at cum. Voluptatum, soluta error iure alias, et quasi deserunt repudiandae ipsum nostrum eum facilis dolor, ipsam qui corporis. Eligendi?
                    Ipsum, reiciendis ducimus nam numquam voluptates sunt earum illum distinctio, modi sapiente quasi corrupti aliquam fuga quos iste nulla rem est, non praesentium! Nam reiciendis asperiores eaque nulla aspernatur voluptatum!
                    Similique voluptas dolorum magnam repellendus ea mollitia molestias accusamus perspiciatis, est ipsam rerum nihil commodi earum omnis pariatur, necessitatibus laudantium, eligendi quae cumque libero blanditiis. Laboriosam labore ea nemo qui!
                    At magnam cumque quis facere! Voluptas, cumque rerum eveniet distinctio deleniti neque incidunt et magni dolor sequi similique, quaerat eaque labore atque voluptatibus debitis error repellat ipsum facilis? Molestiae, ducimus.
                    Tempora dolorum odio fugit. Numquam quis, debitis ipsum velit dignissimos omnis alias animi, dolore, necessitatibus consequuntur voluptatem? Rem amet id atque minus vero officia obcaecati assumenda. Perspiciatis magnam quasi placeat?
                    Incidunt molestiae eius dicta voluptatum nam at quaerat voluptas cum, nostrum praesentium itaque dolores omnis error voluptate. Commodi numquam sunt iusto modi, autem dolore deleniti rem, rerum molestias hic qui.
                    Quam soluta suscipit dolor possimus iure, amet atque accusamus voluptates ipsa ut, reiciendis, velit omnis perferendis sit dolorem natus nesciunt enim? Nostrum dolor dignissimos sunt accusantium distinctio exercitationem nobis corrupti.
                    Distinctio eius sit, similique ducimus quod consequatur at itaque, sunt aliquam magnam voluptas nihil recusandae ea porro, ex iste quibusdam accusantium nesciunt aut quo adipisci odio temporibus aliquid! Illum, cum?
                    Impedit rerum nemo rem, aliquid reprehenderit natus veniam distinctio voluptatibus pariatur porro a numquam voluptatum ratione incidunt dolores hic alias et praesentium reiciendis quisquam odio quibusdam optio. Consequuntur, accusantium numquam!
                    Repellat obcaecati soluta labore ipsa. Ullam, natus rerum corporis ex ratione reprehenderit. Consequatur cupiditate deleniti dolor necessitatibus corporis vel sequi impedit eum eaque? Minima similique repudiandae nemo dolorum quasi cupiditate.
                    Dolores, eveniet! Tempora corrupti possimus hic cupiditate a repellat sint officiis omnis saepe quos. Voluptate dolorum tempore incidunt sit, totam, illo porro suscipit exercitationem voluptatum doloribus placeat quo perspiciatis excepturi.
                    Consequatur, beatae? Corporis sequi magnam temporibus repellendus soluta nam quaerat, nisi recusandae molestiae veritatis dicta nulla cupiditate animi excepturi quod harum quidem quae consequatur explicabo nemo, repudiandae accusantium aliquam? Earum!
                    Officia, illo tenetur! Placeat nemo, magni adipisci culpa ullam molestiae voluptatem, omnis cumque possimus, impedit voluptate porro! Vel, perspiciatis quaerat, optio ipsum atque eius necessitatibus voluptatibus sed unde soluta sequi.
                    Cum perspiciatis dolor excepturi repudiandae, dolore animi magni nulla repellendus! Molestias, eos doloremque? Esse alias iure aliquam? Consequatur laudantium rerum architecto, nemo harum velit alias ipsum nisi magnam sapiente error.
                    Accusantium tempore explicabo placeat quaerat. Laudantium, doloribus! Laborum doloremque necessitatibus voluptate exercitationem amet maiores voluptatibus explicabo doloribus dolor tempore. Dolorum eum voluptates quae? Consequatur voluptates nisi nesciunt maiores consectetur iure.
                    Porro, esse ipsum deserunt ea neque aliquam sunt quasi officiis velit quisquam quod ullam, dolorum similique consequatur! Exercitationem nihil distinctio dolorum quisquam beatae consequuntur itaque aspernatur. Delectus natus ab exercitationem.
                    Recusandae doloremque eaque aspernatur, a pariatur voluptatem molestiae illum, itaque rerum laudantium dignissimos magni id velit nesciunt placeat praesentium ipsam provident saepe mollitia voluptatum possimus voluptatibus. Minus autem iusto unde.
                    Repellat alias culpa blanditiis error eius ut qui cum similique deleniti, eligendi sit facere quaerat dolorum vitae. Inventore adipisci dolore, illum assumenda necessitatibus voluptas, quis minima quos consectetur nam dolorem.
                    Magni atque perferendis quibusdam velit sit culpa reprehenderit quam beatae veniam facilis eveniet, non saepe nostrum incidunt aspernatur? Sit maxime nemo dolorem asperiores fuga. Autem consectetur quisquam recusandae dolore quae.
                    Perferendis porro aspernatur maiores voluptatem quis? Fugit voluptatem architecto impedit, dignissimos voluptatibus ex sunt. Alias non amet voluptatem? Ducimus adipisci veniam, eius esse sunt ea vel! Recusandae quaerat itaque minus.
                    Dolore est ipsa repellat ipsum officia nihil blanditiis? Possimus veritatis laboriosam consequuntur, nulla blanditiis facilis consectetur exercitationem deserunt aliquam officiis rem velit maxime, eaque quos voluptas fugit animi alias sed!
                    Placeat distinctio numquam veritatis repellendus quia libero id quos labore odit alias nemo dolorum eius pariatur culpa sunt quod earum adipisci quo, magni sed? Harum vel odio ratione excepturi hic.
                    Aliquid ad tenetur quas assumenda molestias, exercitationem perspiciatis deleniti facilis excepturi, laboriosam similique minus voluptatibus pariatur dolore numquam veniam repellat, non recusandae! Error est accusamus placeat praesentium explicabo, ipsa perferendis.
                    Eaque saepe, totam necessitatibus obcaecati quaerat asperiores sint quas est repellat dolorum harum accusamus error omnis inventore fuga illum. Animi harum officiis ex neque fugiat aliquid delectus quisquam sequi eius.
                    Nobis alias ea tempore! Quam error illum harum accusantium quasi quibusdam quidem, perspiciatis fugit vel deleniti, est ratione! Laborum porro corrupti ut quibusdam dolores maxime molestias repellat commodi provident eum?
                    Perferendis quasi iusto optio ratione, alias commodi dolore harum vitae nostrum temporibus voluptatibus recusandae neque aliquam eos autem possimus nemo omnis ipsa unde sit id voluptatum dolorum molestias aspernatur. Totam.
                    Tempora doloribus ad nemo, quaerat incidunt rem nihil dignissimos sequi fugit laudantium vitae blanditiis expedita praesentium. Quibusdam unde voluptatem quasi, deleniti est vero architecto temporibus nostrum velit minus, perspiciatis maiores.
                    Ut exercitationem hic, non esse ad, maiores labore, ratione voluptatibus magnam nostrum repellendus? Mollitia, tempore corrupti beatae quam cumque possimus nulla nobis quos rem, sequi omnis praesentium, ex aliquid dolorem.
                    Soluta aliquid in molestiae eveniet veniam inventore, illo doloremque voluptatibus, quas voluptates distinctio nisi itaque pariatur suscipit ea aperiam placeat quam perferendis natus debitis culpa. Ipsum ratione nisi veniam accusantium?
                    Laboriosam nesciunt explicabo culpa! Quibusdam molestias rerum labore porro doloremque totam ducimus assumenda, sequi deserunt suscipit dolorum nihil illum maiores, quam, cum eligendi voluptas hic recusandae libero ratione modi aliquid?
                    Earum itaque doloribus perspiciatis corrupti nihil aut quas sapiente animi ab ducimus illo sequi suscipit, dicta similique corporis odio laborum enim. Necessitatibus, et repellat ducimus animi explicabo repellendus voluptatibus. Necessitatibus?
                    Fuga eos ad doloremque inventore reprehenderit accusamus ab, magni voluptatibus magnam omnis aliquid, asperiores odio? Quae nulla optio minus ipsam provident sunt, hic esse, illo praesentium excepturi aliquam nam iure.
                    Quam dolores odio dolorum, eius expedita quibusdam iusto excepturi ex nulla consequuntur ipsum totam quos cum incidunt alias rem velit molestias ratione earum itaque hic distinctio sunt accusantium provident! Explicabo?
                    Nihil inventore neque reiciendis alias distinctio laboriosam numquam magnam provident? Molestias hic dignissimos debitis deserunt? Cum ex omnis, eius itaque vero debitis animi ratione voluptate odit commodi consectetur vitae alias.
                    Mollitia necessitatibus autem reiciendis placeat natus aspernatur consequatur quasi modi animi ratione quos nam nobis officia consequuntur dolorum ipsam voluptate molestias non rem libero, similique qui. Tempore aperiam obcaecati doloribus!
                    Nisi repellendus sed quibusdam animi nulla, exercitationem molestias sint ullam! Commodi aliquid sed vero esse ullam dolore adipisci facilis eum blanditiis, minima necessitatibus quia totam fugit sit quibusdam. Quos, neque?
                    Temporibus autem ad fugit nihil tenetur pariatur itaque nulla blanditiis ullam sapiente sint, nostrum earum veniam corporis quibusdam natus ipsam nisi eveniet excepturi, at dolorem praesentium esse sequi! Adipisci, repellendus!
                    Quod eligendi ut, vitae veniam aut at est quidem porro nostrum. Eveniet magnam libero, laudantium nihil corporis dolor repellat vero tenetur, fuga cupiditate vitae est, natus sed a illum molestiae.
                    Laudantium doloribus assumenda doloremque dolor voluptatem veniam exercitationem dignissimos! Beatae facere vitae ipsum molestias. Suscipit corrupti soluta consequuntur alias quod ratione similique placeat quam consectetur architecto earum fugiat, at debitis.
                    Magnam sint adipisci, minima cupiditate quam nostrum quasi aut praesentium inventore repellat assumenda necessitatibus id itaque alias obcaecati nisi esse in quibusdam rerum vero placeat iure velit? Illum, doloribus temporibus.
                </p>
            </Panel>
        )
     }

    render() { 
        return (
            <> 
                <section>
                    {this.showDetail()}
                </section>
                <section>
                    {this.showDescription()}
                </section>
            </>
         );
    }
}
 
export default About;
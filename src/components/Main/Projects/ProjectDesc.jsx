import React from "react";
import Panel from '../Panel'

function showLanguage(language){
    const lang = language.toLowerCase();
    const icon = require(`../../../assets/img/projects/languages/${lang}.svg`);
    return(
        <div className={'lang lang_'+lang}>
            <img src={icon} alt={lang}/>
            <p>{lang}</p>
        </div>
    )
 }

const ProjectDesc = (props) => {
  const { site, desc, id, lang, onClose, children } = props;
  return (
    <Panel name={site+" prodesc"} enable={desc} id={id} onClose={onClose}>
      <div className="pinfo">
        <h1 className="ptitle">{site}</h1>
        {children}
      </div>
      <div className="ptech">
        <h3>Languages used:</h3>
        <div className="langusd">{lang.map(i => showLanguage(i))}</div>
      </div>
    </Panel>
  );
};

export default ProjectDesc;

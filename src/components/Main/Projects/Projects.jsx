import React, { Component } from "react";
import Window from "../Window";
import Button from "../../Button";
import ProjectDesc from "./ProjectDesc";
import "../../../scss/Projects.scss";

class Projects extends Component {
  constructor(props) {
    super(props);
    this.state = {
      softwares: [
        {
          id: 0,
          name: "Rallx Booter",
          thumb: 0,
          time: "Aug 2019",
          lang: ["Python", "PyQT5"],
          clicked: false
        },
        {
          id: 1,
          name: "Rallx Hider",
          thumb: 0,
          time: "Oct 2019",
          lang: ["Python", "PyQT5"],
          clicked: false
        }
      ],
      websites: [
        {
          id: 0,
          site: "Typometer",
          thumb: 1,
          time: "July 2018",
          lang: ["HTML", "CSS", "jQuery"],
          about: this.descTypometer,
          clicked: false
        },
        {
          id: 1,
          site: "Tekzion",
          thumb: 1,
          time: "Feb 2018",
          lang: ["PHP", "CSS", "jQuery"],
          about: this.descTekzion,
          clicked: false
        }
      ],
      desc: 0,
      type: 1
    };

    this.handleIconClick = this.handleIconClick.bind(this);
    this.getToTop = this.getToTop.bind(this);
  }

  handleItemClick(event) {
    const ref = event.target.className;
    const obj = ref.split(" ");
    const id = obj[0];
    const ele = obj[1];
    const { desc, item } = this.state;
    if (desc) {
      if (desc == "websites") {
        this.setState(prevState => {
          const websites = this.prevState.websites.map(i => {
            if (i.id == item) {
              i.clicked = false;
            }
            return websites;
          });
          return websites;
        });
      }
    }
    this.setState(prevState => {
      const websites = this.prevState.websites.map(i => {
        if (i.id == id) {
          i.clicked = true;
        }
        return websites;
      });
      return websites;
    });
  }

  handleIconClick(event) {
    const ref = event.target.className;
    const data = ref.split(" ");
    const id = data[0];
    const type = data[1];
    if(type == 1 || type == 0){
      this.props.onIconClick(this.props.id,1,true);
      this.setState({
        desc: id,
        type: type
      });
    }
  }

  getProjects(type) {
    let projects;
    if (type) {
      const { id, name, clicked } = this.state.softwares;
      const logo = name.toLowerCase();
      const icon = require(`../../../assets/img/projects/${logo}.png`);
      projects = this.state.softwares.map(i => (
        <Button
          id={id}
          name={"software" + id}
          btn_image={icon}
          shape="empty"
          size={32}
        />
      ));
    } else {
      projects = this.state.websites.map(i => this.showProject(i.id, i.site));
    }
    return projects;
  }

  showProject(id, site) {
    const logo = site.toLowerCase();
    const icon = require(`../../../assets/img/projects/${logo}/${logo}.png`);
    return (
      <div className={id + " pro"} onClick={this.handleIconClick}>
        <img className={id + " 1"} src={icon} alt={site} />
        <p className="sitename">{site}</p>
      </div>
    );
  }

  getToTop(){
    const { id, setToTop } = this.props;
    setToTop(id);
  }

  showGallary() {
    const { det, desc, page, id, onClose, top } = this.props;
    return (
      <Window name={page} id={id} enable={det} onClose={onClose} top={top} getToTop={this.getToTop}>
        <div className="softwares">
          <h1>Softwares</h1>
          <div className="gallary">{/* {this.getProjects(1)} */}</div>
        </div>
        <div className="websites">
          <h1>Web Projects</h1>
          <div className="gallary">{this.getProjects(0)}</div>
        </div>
      </Window>
    );
  }

  projectDesc(pro) {
    const { desc, id, onClose } = this.props;
    const { site, lang, time, thumb, about } = this.state.websites[pro];
    return (
      <ProjectDesc
        site={site}
        desc={desc}
        id={id}
        lang={lang}
        time={time}
        thumb={thumb}
        onClose={onClose}
      >
        {about()}
      </ProjectDesc>
    );
  }

  descTypometer() {
    return (
      <>
        <p>
          A website to check your typing speed. You have a option to set
          duration of the test and also for those who forget themself when
          typing I got you guys covered too, there is a option called free mode
          which does not have a duration.{" "}
          <span className="bold">
            To check your speed you need to atleast take the test for a minute
          </span>
          , well you do want your speed in WPM aren't you :)
        </p>
      </>
    );
  }

  descTekzion() {
    return (
      <>
        <p>Website I designed for my college symposium.</p>
      </>
    );
  }

  showDesc(id, type) {
    if (type == 1) {
      return this.projectDesc(id);
    }
  }

  render() {
    const { desc, type } = this.state;
    return (
      <section>
        {this.showGallary()}
        {this.showDesc(desc, type)}
      </section>
    );
  }
}

export default Projects;

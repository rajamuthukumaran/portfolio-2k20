import React, { Component } from "react";
import Button from "../Button";
import "../../scss/Panel.scss";

class Panel extends Component {
  render(props) {
    const { name, children, onClose, enable, id } = this.props;
    let hide = ''
    if(!enable){
        hide = ' hide'
    }
    return (
      <div className={id + " pnl pnl_" + name + hide}>
        <Button
          name="pnl_clse"
          onBtnClick={onClose}
          id={id}
          size={15}
          shape='round'
          type='desc'
          btn_img={require("../../assets/img/close.svg")}
        />
        {children}
      </div>
    );
  }
}

export default Panel;

import React from 'react';
import '../scss/Button.scss'

const Button = (props) => {

    const { size, onBtnClick, name, btn_img, shape, type, id} = props

    const btn_sz = size/2;
    const btn_bg = `url(${btn_img}) no-repeat center /${btn_sz}px`;
    const btn_style = {
        width: size,
        height: size,
        background: btn_bg,
    }

    let ele = ''
    if(type){
        ele = " "+type;
    }

    return ( 
        <div style={btn_style} className={id+ele+" btn "+name+" "+shape} onClick={onBtnClick}>

        </div>
     );
}
 
export default Button;
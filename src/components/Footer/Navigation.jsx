import React from 'react';
import Button from '../Button';
import '../../scss/Navigation.scss'

const navButtons = (props) => {
    const { pages, onClick } = props
    const buttons = pages.map( i => {
        return (

            <Button name={"nav_btn nav_"+i.page} size={42} id={i.id} key={i.id} shape="round" onBtnClick={onClick}/>
        )
    })
    return buttons;
}

const Navigation = (props) => {
    return ( 
        <nav>
            <div className="dock">
                {navButtons(props)}
            </div>
        </nav>
     );
}
 
export default Navigation;
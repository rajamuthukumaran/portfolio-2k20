import React, { Component } from 'react';
import '../../scss/News.scss'

class News extends Component {
    state = { 
        isLoaded: false,
        news: []
     }

    componentDidMount(){
        const API_KEY = process.env.REACT_APP_NEWS_API_KEY;
        const API = `https://api.nytimes.com/svc/topstories/v2/technology.json?api-key=${API_KEY}`;

        fetch(API)
            .then(res => res.json())
            .then(data => {
                const code = data.status;
                const news = data.results;
                if(code == 'OK'){
                    this.setState({
                        isLoaded: true,
                        news: news
                    })
                }
                else{
                    this.setState({
                        isLoaded: false
                    })
                }
            });
    }

    getheadlines(){
        let headlines = ''
        this.state.news.map(i =>
            headlines += `${i.title}, ${i.abstract} | `
        );
        return headlines;
    }

    render() {
        const headlines = this.getheadlines();
        return (
            <div className="news">
                <p className="headlines">
                    {headlines}
                </p>
            </div>
         );
    }
}
 
export default News;